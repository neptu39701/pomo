package com.example.yuri.pomo;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class BlankFragment extends Fragment {
    boolean isTimerRunning = false;
    private Button startControlButton;
    private Button resetButton;
    private TextView timerValue;
    private CountDownTimer countDownTimer;
    boolean isOnBreak = false;

    public BlankFragment() {
    }

    public static BlankFragment newInstance(int focusTime, int restTime) {

        BlankFragment fragment = new BlankFragment(); //initialing fragment

        Bundle args = new Bundle();
        long breakTimeLeft = restTime * 1000; //Break Time
        long timeLeftMillies = focusTime * 12000;//focus time to seconds
        args.putLong("restRemaining", breakTimeLeft);
        args.putLong("focusRemaining", timeLeftMillies);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);

        resetButton = view.findViewById(R.id.Btn_reset);
        startControlButton = view.findViewById(R.id.Btn_start);
        timerValue = view.findViewById(R.id.timerText);

        startControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isTimerRunning)
                    pauseTimer();
                else if (isOnBreak)
                    pauseTimer();
                else
                    startTimer();

            }
        });
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //to reset timer to zero
                resetTimer();
            }
        });

        updateTimerText(getArguments().getLong("focusRemaining"));

        return view;

    }

    //to start count down  timer
    private void startTimer() {

        countDownTimer = new CountDownTimer(getArguments()
                .getLong("focusRemaining"),
                1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                updateTimerText(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                isTimerRunning = false;
                startControlButton.setText("START");
                startBreak();
                isOnBreak = true;
                resetButton.setVisibility(View.VISIBLE);
            }
        }.start();
        resetButton.setVisibility(View.INVISIBLE);

        isTimerRunning = true;
        startControlButton.setText("Pause");
    }

    private void pauseTimer() {
        countDownTimer.cancel();
        isTimerRunning = false;
        startControlButton.setText("Start");
        resetButton.setVisibility(View.VISIBLE);
        isOnBreak = false;

    }

    private void resetTimer() {
        updateTimerText(getArguments().getLong("focusRemaining"));
        countDownTimer.cancel();
        resetButton.setVisibility(View.INVISIBLE);

    }

    private void updateTimerText(long timeLeftMillies) {
        int minutes = (int) (timeLeftMillies / 1000) / 60;
        int seconds = (int) (timeLeftMillies / 1000) % 60;

        String timeLeftFormat = String.format(Locale.getDefault(),
                "%02d:%02d", minutes, seconds);
        timerValue.setText(timeLeftFormat);
    }

    private void startBreak() {
        countDownTimer = new CountDownTimer(10000,
                1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                updateTimerText(millisUntilFinished);

            }

            @Override
            public void onFinish() {

                startTimer();
                isTimerRunning = true;
            }
        }.start();


    }

}

